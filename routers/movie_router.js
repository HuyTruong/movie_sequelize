const express = require('express');
const movieRouters = express.Router();
const { createMovie, getListMovie, getDetailMovie, deleteMovie, updateMovie } = require('../controllers/movie_controller.js');

// create movie
movieRouters.post("/", createMovie);
// get list movie
movieRouters.get("/", getListMovie);
// get detail movie
movieRouters.get("/:id", getDetailMovie);
// delete movie
movieRouters.delete("/:id", deleteMovie);
// update movie
movieRouters.put("/:id", updateMovie);
    
module.exports = {
    movieRouters,
}