const express = require("express");
const { movieRouters } = require("./movie_router");
const rootRouters = express.Router();

rootRouters.use("/movie", movieRouters);

module.exports = {
    rootRouters
}
