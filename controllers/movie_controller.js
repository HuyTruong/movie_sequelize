const { Movie } = require('../models/index.js');

const createMovie = async (req, res) => {
    const { name, totalMovietime, poster, trailer } = req.body;
    try {
        const newMovie = await Movie.create({ name, totalMovietime, poster, trailer });
        res.status(200).send(newMovie);
    } catch (error) {
        res.status(500).send(error);
    }
}

const getListMovie = async (req, res) => {
    try {
        const listMovie = await Movie.findAll();
        res.status(200).send(listMovie);
    } catch (error) {
        res.status(500).send(error);
    }   
}

const getDetailMovie = async (req, res) => {
    const { id } = req.params;
    try {
        const detailMovie = await Movie.findByPk(id)
        if (detailMovie) {
            res.status(200).send(detailMovie);
        } else {
            res.status(404).send("Not found!");
        }
    } catch (error) {
        res.status(500).send(error);
    }
}

const deleteMovie = async (req, res) => {
    const { id } = req.params;
    try {
        await Movie.destroy({
            where: {
                id
            }
        });
        res.status(200).send("Delete success!");
    } catch (error) {
        res.status(500).send(error);
    }
}

const updateMovie = async (req, res) => {
    const { name, totalMovietime, poster, trailer } = req.body;
    const { id } = req.params;
    try {
        const [countUpdate] = await Movie.update(
            { name, totalMovietime, poster, trailer },
            { where: { id } }
        );
        if (countUpdate > 0) {
            res.status(200).send("Update success!");
        } else {
            res.status(404).send("Not found!");
        }
    } catch (error) {
        res.status(500).send(error);
    }
}
module.exports = {
    createMovie,
    getListMovie,
    getDetailMovie,
    deleteMovie,
    updateMovie
}