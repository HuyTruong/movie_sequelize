const express = require("express");
const chalk = require("chalk");
const { rootRouters } = require("./routers/root_router");

const app = express();

app.use(express.json());
app.use("/api", rootRouters)

const port = 700;
app.listen(port, () => {
    console.log(chalk.blue(`app running with ${port}`));
})
